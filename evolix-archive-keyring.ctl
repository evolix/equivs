Package: evolix-archive-keyring
Version: 0~2024+deb12u2
Description: GnuPG archive keys of the Evolix archive
 The Release files from pub.evolix.org are digitally signed. This
 package contains the archive keys to verify them.
Files: pub_evolix.asc /etc/apt/trusted.gpg.d/
 pub_evolix.asc /etc/apt/keyrings
 pub_evolix.asc /usr/share/keyrings
 pub_evolix_auto.asc /usr/share/keyrings
