Package: serveur-base
Depends: ssh, vim, ntp | time-daemon, sudo, munin, munin-node, log2mail, less, bsd-mailx, logcheck, logcheck-database, screen, git, libpam-systemd, rsyslog, evolix-archive-keyring, jq
Version: 0.5.4
Description: Evolix 'serveur' installation components
 This metapackage provides the essential components for
 an installation of a Pack Evolix server.
Files: logcheck/evolix_courier /etc/logcheck/ignore.d.server/
 logcheck/evolix_iptables /etc/logcheck/ignore.d.server/
 logcheck/evolix_log2mail /etc/logcheck/ignore.d.server/
 logcheck/evolix_mysql /etc/logcheck/ignore.d.server/
 logcheck/evolix_nrpe /etc/logcheck/ignore.d.server/
 logcheck/evolix_ntpd /etc/logcheck/ignore.d.server/
 logcheck/evolix_php /etc/logcheck/ignore.d.server/
 logcheck/evolix_proftp /etc/logcheck/ignore.d.server/
 logcheck/evolix_rsync /etc/logcheck/ignore.d.server/
 logcheck/evolix_rsyslogd /etc/logcheck/ignore.d.server/
 logcheck/evolix_saslauthd /etc/logcheck/ignore.d.server/
 logcheck/evolix_snmpd /etc/logcheck/ignore.d.server/
 logcheck/evolix_spamd /etc/logcheck/ignore.d.server/
 logcheck/evolix_ssh /etc/logcheck/ignore.d.server/
Changelog: serveur-base.changelog
