Package: php-symfony-polyfill-php80
Depends: php-common (>= 80:80)
Version: 1.26
Description: Workaround broken backport of php-slim-psr7
 php-slim-psr7 depends on (recent) php-symfony-polyfill-php80 by mistake.
Files: php-symfony-polyfill-php80/autoload.php /usr/share/php/Symfony/Polyfill/Php80/
