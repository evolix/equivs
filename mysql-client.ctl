Package: mysql-client
Depends: default-mysql-client
Version: 8.0
Description: Workaround MySQL client for Ægir
 Current aegir3-cluster-slave package has unfulfillable dependencies on (at
 least) Bullseye.
