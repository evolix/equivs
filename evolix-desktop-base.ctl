Package: evolix-desktop-base
Recommends: apg,apt-file,arping,asciidoc,audacious,bc,bsdgames,build-essential,devscripts,dlocate,dnsutils,docbook,docbook-xsl,elinks,ftp,git,gnupg,gpm,gv,hdparm,hevea,host,htop,firefox-esr,firefox-esr-l10n-fr,thunderbird,thunderbird-l10n-fr,ipcalc,irssi,keychain,ldap-utils,ldapvi,less,lftp,libnss-ldap,libpam-ldap,libtiff-tools,locate,lpr,lynx,manpages-dev,minicom,mtr,mutt,nfs-common,nmap,ntfs-3g,ntpdate,libreoffice,libreoffice-l10n-fr,openvpn,password-gorilla,pdftk,perl-doc,pidgin,postfix,procmail,rdesktop,rxvt-unicode,screen,ssh,ssh-askpass,sshfs,streamtuner2,t1-cyrillic,tcpdump,telnet,texlive,texlive-latex-extra,traceroute,trickle,vim,vlc,whois,wodim,xfce4,xfce4-clipman-plugin,xpdf,xsane,xscreensaver,xtrlock,zip,sxiv,clusterssh
Version: 0.1
Description: Evolix 'desktop' installation components
 This metapackage provides the essential components for
 an installation of an Evolix desktop computer.
Changelog: evolix-desktop-base.changelog
